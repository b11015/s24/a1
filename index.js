let getCube = 5 ** 3
console.log(getCube)
console.log(`The cube of 5 is ${getCube}.` )

const address = ["258", "Washington Ave NW", "California", "90011"];
let houseNumber = 258;
let street = "Washington Ave NW";
let city = "California";
let code = "90011"

fullAdress = `I live at ${houseNumber} ${street} ${city} ${code}`
console.log(fullAdress)

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};

console.log(`${animal.name} ${animal.species}. He weighed at ${animal.weight} with a measurement of ${animal.measurement}`)

const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

characters.forEach((characters) => console.log(`${characters}`));

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog();

	myDog.name = 'Mia';
	myDog.age = '2';
	myDog.breed = 'Poodle';

console.log(myDog)


const newDog = new Dog("Tatum", "1", "Shiba Inu");
console.log(newDog)